\documentclass[12pt, a4paper, tikz]{article}
\usepackage{amsthm,amsfonts,amsmath,amssymb,amscd}
\usepackage[T2A]{fontenc}                         
\usepackage[utf8]{inputenc}                      
\usepackage[english, russian]{babel}
%\usepackage[final]{graphics}
\usepackage{enumitem}
\usepackage{graphicx}
\usepackage{indentfirst}
%\usepackage{misccorr}
\usepackage{cite} 
\usepackage{psfrag}
\usepackage{pgfplots}
\usepackage{bm}
\usetikzlibrary{intersections, calc}
\usepackage[linesnumbered,boxed]{algorithm2e}
\usepackage{multirow}
\usepackage{hyperref}
\pgfplotsset{compat=1.11}
\definecolor{light-gray}{gray}{0.98} 
\IfFileExists{pscyr.sty}{\usepackage{pscyr}}{}    %
\usepackage[top=2cm,bottom=2cm,left=2cm,right=2cm]{geometry}
\usepackage{subcaption}
\usepackage{mathtools}
\usepackage{accents}

%\usepackage[svgnames]{xcolor}
  \definecolor{diffstart}{named}{gray}
  \definecolor{diffincl}{named}{green}
  \definecolor{diffrem}{named}{red}

\usepackage{listings}
  \lstdefinelanguage{diff}{
    basicstyle=\ttfamily\small,
    morecomment=[f][\color{diffstart}]{@@},
    morecomment=[f][\color{diffincl}]{+\ },
    morecomment=[f][\color{diffrem}]{-\ },
  }
\DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
\newtheorem{theorem}{Теорема}[section]
\newtheorem{corollary}{Следствие}[section]
\linespread{1.3} 
\urlstyle{same}

\begin{document}
\begin{titlepage}
\begin{center}
\includegraphics[width=10cm, height=6cm]{MSU}
\end{center}
\begin{center}
Московский государственный университет имени М.В. Ломоносова\\

\vspace{3cm}

    %{\bf\Large Задание 2}\\ \vspace{1cm}
    {\bf\Large Исследование и численное решение одной задачи о начальном состоянии для волнового уравнения
    }
\end{center}
\vspace{2cm}
\begin{flushright}

{\bf Факультет:} Вычислительной математики и кибернетики\\
{\bf Кафедра:} Вычислительных технологий и моделирования\\
    {\bf Предмет:} Сопряженные уравнения и методы оптимального управления\\
    {\bf Студент:} Тимофеев Александр Евгеньевич\\
    {\bf Группа:} 603\\

    

\end{flushright}

 \vspace{7.5cm}

\centerline {Москва, 2020}

\end{titlepage}

\newpage
\setcounter{page}{2}
\tableofcontents
%\listoftables



\newpage


\section{Исходная постановка задачи}
В  прямоугольной области
$
\Omega = (0, A) \times (0, B)
$
с границей $\partial \Omega$
на временном интервале $\left(0, T < \infty \right)$
рассматривается волновое уравнение
\begin{equation} \label{waveEq}
\frac{\partial^2\phi}{\partial t^2} - \mu \Delta \phi  + b \phi = f(t,x,y),
\end{equation}
где известны $\mu = const > 0$, $b = const \geq 0$, $f$ -- заданная функция правой части. Уравнение дополняется граничным условием
\begin{equation} \label{boundaryCond}
\left. \phi \right|_{\partial \Omega} = 0 , \text{ } \forall t \in \left[0, T\right].
\end{equation}
По известному состоянию $(\phi^{(0)}_{obs},\phi^{(1)}_{obs})$  в момент времени $t=T$:
\begin{equation} \label{endState0}
\phi(x, y, T) = \phi^{(0)}_{obs}(x,y), \text{  } (x,y) \in \Omega,
\end{equation}
\begin{equation} \label{endState1}
\frac{\partial \phi}{\partial t}(x,y,T) = \phi^{(1)}_{obs}(x,y), \text{  } (x,y) \in \Omega,\\
\end{equation}
 требуется определить начальное состояние $(u_0, u_1)$ в момент времени $t=0$:
\begin{equation} \label{startState0}
\phi(x, y, 0) = u_{0}(x,y), \text{  } (x,y) \in \Omega,
\end{equation}
\begin{equation} \label{startState1}
\frac{\partial \phi}{\partial t}(x,y,0) = u_{1}(x,y), \text{  } (x,y) \in \Omega,\\
\end{equation}
и решение $\phi$ на $Q_T = \Omega \times (0, T)$.

\section{Классическая постановка прямой задачи}
Приведем классическую постановку прямой задачи.
Требуется найти функцию $\phi(x,y,t) \in C^2(Q_T) \cap C^1(\overline{Q}_T)$, удовлетворяющую уравнению \ref{waveEq}
в цилиндре $Q_T$, известным начальным условиям \ref{startState0}, \ref{startState1} и граничному условию \ref{boundaryCond}.
При этом должны быть выполнены условия гладкости $f \in C(Q_T), ~~ u_0 \in C^1(\overline{\Omega}), ~~ u_1 \in C(\overline{\Omega})$ и условия согласованности $\left. u_0 \right|_{\partial \Omega} = \left. u_1 \right|_{\partial \Omega} =  0$. Однако не всегда можно предполагать достаточную гладкость известных данных, и в таких
случаях следует рассматривать обобщенную постановку.

\section{Обобщенная постановка прямой задачи}
Приведем сперва обобщенную постановку прямой задачи, а затем перепишем ее в виде операторного уравнения.
Здесь и далее все рассматриваемые пространства вещественны.
Рассмотрим $H = L_2(\Omega)$. Представим $f(t,x,y)$ как элемент пространства $L_2((0,T);H)$, то есть как функцию, отображающую $t \in (0,T)$ в функцию из $H$. Также рассмотрим $X = \accentset{\circ}{W}^1_2(\Omega)$ и
обобщенным решением будем считать функцию $\phi \in L_2([0,T];X)$, удовлетворяющую \ref{waveEqWeak} -- \ref{startState1Weak}.
Умножая \ref{waveEq} на $\nu \in X$ и интегрируя по $\Omega$ при фиксированном $t$ с использованием формулы Грина, получаем
\begin{equation} \label{waveEqWeak}
\int_{\Omega} \phi_{tt} \nu \,d\Omega + \int_{\Omega} \left( \mu \nabla \phi
    \cdot \nabla \nu + b \phi \nu \right) \,d\Omega = \int_{\Omega}f\nu \,d\Omega ~~ \forall \nu \in X, ~~ \forall t \in \left(0, T\right).
\end{equation}
В качестве начальных условий будем рассматривать
\begin{equation} \label{startState0Weak}
\left.\phi\right|_{t=0} = u_{0}, ~~ u_0 \in X,
\end{equation}
\begin{equation} \label{startState1Weak}
\left.\frac{d \phi}{d t}\right|_{t=0} = u_{1}, ~~ u_1 \in H.\\
\end{equation}

Выпишем операторную форму обобщенной постановки.
Рассмотрим билинейную форму $a(\phi,\psi)$ на элементах пространства $X$
\begin{equation} \label{form}
a(\phi, \psi) = \int_{\Omega} \left( \mu \nabla \phi \cdot \nabla \psi + b \phi \psi \right) \,d\Omega.
\end{equation}
Согласно теореме Лакса--Мильграма, данная форма порождает ограниченный оператор $A: X \rightarrow X^{*}$
\begin{equation}
\langle A\phi, \psi \rangle = a(\phi, \psi) ~~ \forall \phi, \psi \in X.
\end{equation}
Теперь задача представима в виде задачи Коши для дифференциального операторного уравнения:
\begin{equation} \label{operWave}
\phi_{tt} + A \phi = f, ~~ \forall t \in \left(0, T\right),
\end{equation}
\begin{equation} \label{oper0}
\left.\phi\right|_{t=0} = u_{0},
\end{equation}
\begin{equation} \label{oper1}
\left.\frac{d \phi}{d t}\right|_{t=0} = u_{1}.\\
\end{equation}

\par
Введем в рассмотрение банахово пространство $C^{(0)}([0,T];H)$ функций $f(t)$ над $[0,T]$ с нормой 
\begin{equation}
\norm{f}_{C^{(0)}([0,T];H)} = \max_{t \in [0,T]} \norm{f}_H .
\end{equation}
Известно следующее утверждение:
\begin{theorem}[\cite{lions1}, \cite{lions2}]
Существует единственное решение решение $\phi$ задачи \ref{operWave} -- \ref{oper1} такое, что
\begin{itemize}
\item $\phi \in L_2((0,T);X), ~\phi_t \in L_2((0,T);H),~ \phi_{tt} \in L_2((0,T);X^*)$;
\item $\Phi \equiv (\phi, \phi_t)^T \in C^{(0)}([0,T];X) \times C^{(0)}([0,T];H)$;
\item отображение $\{f, u_0, u_1\} \rightarrow  \Phi$ непрерывно действует из $L_2((0,T);H) \times X \times H$
в $C^{(0)}([0,T];X) \times C^{(0)}([0,T];H) \equiv Y$.
\end{itemize}
\end{theorem}

\begin{corollary}
Решение $\phi$ задачи \ref{operWave} -- \ref{oper1} представимо в виде
\begin{equation}
\phi = G_0 u_0 + G_1 u_1 + Gf,
\end{equation}
где $G_0, G_1, G$ -- линейные ограниченные операторы:
$$
G_0 \in \mathcal{L}(X; Y),~ G_1 \in \mathcal{L}(H;Y), ~G \in \mathcal{L}(L_2((0,T);H);Y).
$$
\end{corollary}

\begin{corollary} \label{col1}
Для решения  $\phi$ задачи \ref{operWave} -- \ref{oper1} верно
\begin{equation}
T_1 \phi = (T_1 G_0 u_0 + T_1  G_1 u_1 + T_1 Gf) \in X,
\end{equation}
\begin{equation}
T_1 \ell \phi = (T_1 \ell G_0 \phi + T_1 \ell G_1 u_1 + T_1 \ell Gf) \in H,
\end{equation}
где $ \ell \phi \equiv \phi_t, ~ T_1 \phi \equiv \left.\phi\right|_{t=T}$.
\end{corollary}


\section{Операторная постановка обратной задачи}
Рассмотрим пространство $W$, состоящее из вектор-функций
$$
U = (u_0, u_1)^T \in W ~~ \forall u_0 \in X, ~\forall u_1 \in H
$$
со скалярным произведением
$$
(U,V)_W = \frac{1}{2}\left((u_1, v_1) + a(u_0, v_0)\right) ~~ \forall U = (u_0, u_1)^T, ~\forall V = (v_0, v_1)^T \in W
$$
и нормой
$$
\norm{U}_W^2 = (U,U)_W .
$$
\par
Операторная постановка обратной задачи \ref{waveEq}--\ref{startState1} может быть сформулирована следующим образом:
по заданным $f \in L_2((0,T);H), ~ \Phi_{obs} \equiv (\phi_{obs}^{(0)}, \phi_{obs}^{(1)})^T \in W$
найти $\phi \in L_2([0,T];X), ~U \equiv (u_0, u_1)^T \in W$ такие, что выполнено \ref{operWave} -- \ref{oper1}, а также
$$
\phi(T) = \phi_{obs}^{(0)}, ~ \phi_t(T) = \phi_{obs}^{(1)}.
$$

Согласно следствию \ref{col1} имеем:
$$
\phi_{obs}^{(0)} = T_1 G_0 u_0 + T_1  G_1 u_1 + T_1 Gf
$$
$$
\phi_{obs}^{(1)} = T_1 \ell G_0 \phi + T_1 \ell G_1 u_1 + T_1 \ell Gf
$$

Тем самым начальное состояние $U$ и финальное наблюдение $\Phi_{obs}$ связаны соотношением
\begin{equation} \label{startEndRel}
\mathcal{A}U = \Phi_{obs} - g, ~~ \mathcal{A} = \begin{bmatrix} T_1 G_0 & T_1 G_1 \\ T_1 \ell G_0 & T_1 \ell G_1
\end{bmatrix}
, ~ g = \begin{bmatrix} T_1 G f \\ T_1 \ell G f \end{bmatrix}
\end{equation}


В статье \cite{agoshkovPaper} приведено доказательство того, что $\norm{\mathcal{A} V}_W = \norm{V}_W ~ \forall V \in W$.
Тем самым оператор $\mathcal{A}$ ограничен и $ker\mathcal{A} = \{0\}$. Из последнего следует, что операторное
уравнение \ref{startEndRel} однозначно разрешимо.

В статье \cite{agoshkovPaper} также доказано, что оператор $\mathcal{A}$ является унитарным, при этом
\begin{equation} \label{adjOp}
\mathcal{A}^* = \begin{bmatrix} T_0 \widetilde{G_0} & T_0 \widetilde{G_1} \\ T_0 \ell \widetilde{G_0} & T_0 \ell
\widetilde{G_1}
\end{bmatrix}.
\end{equation}
Оператор $\mathcal{A}^*$ может быть определен через решение задачи
\begin{equation}
q_{tt} + Aq = 0, ~ t \in (0,T)
\end{equation}
\begin{equation}
\left.q\right|_{t=T} = \widetilde{e}_0, ~~ \left.q_t\right|_{t=T} = \widetilde{e}_1, 
~~\forall ( \widetilde{e}_0, ~\widetilde{e}_1)^T \equiv \widetilde{e} \in W,
\end{equation}
при этом
\begin{equation}
\begin{bmatrix} T_0 q \\ T_0 \ell q \end{bmatrix} = 
\mathcal{A}^*
\begin{bmatrix} \widetilde{e}_0 \\ \widetilde{e}_1 \end{bmatrix}
\end{equation}

Плотная разрешимость операторного уравнения \ref{startEndRel} следует из того, что $ker \mathcal{A}^* = \{0\}$.

\section{Регуляризация}
Выпишем метод регуляризации А. Н. Тихонова в применении к уравнению \ref{startEndRel}
\begin{equation}
\alpha U + \mathcal{A}^* \mathcal{A} U = \mathcal{A}^* \left(\Phi_{obs} - g\right),
\end{equation}
что также является условием оптимальности соответствующего функционала.
Так как $\mathcal{A}^* \mathcal{A} = \mathcal{I}$, то
\begin{equation}
U = \frac{1}{1+\alpha}\mathcal{A}^* \left(\Phi_{obs} - g\right).
\end{equation}
Выбор параметра регуляризации $\alpha$ оказывается произвольным, поэтому фиксируем $\alpha = 0$, тем самым необходимо
численно решить задачу
\begin{equation}
U = \mathcal{A}^* \left(\Phi_{obs} - g\right)
\end{equation}
в два этапа:
\par
1. Определение $g = \begin{bmatrix} T_1 G f \\ T_1 \ell G f \end{bmatrix}$ путем решения 
\begin{equation} \label{dir}
\phi_{tt} + A \phi = f, ~~  t \in \left(0, T\right],
\end{equation}
\begin{equation} \label{dirB}
\left.\phi\right|_{t=0} = 0,~~
\left.\phi_{t}\right|_{t=0} = 0.\\
\end{equation}
\par
2. Определение $U$ как результата действия оператора $\mathcal{A}^*$ на $\left(\Phi_{obs} - g\right)$:
\begin{equation} \label{inv}
q_{tt} + Aq = 0, ~~ t \in [0,T),
\end{equation}
\begin{equation} \label{invB}
\left.q\right|_{t=T} = \phi^{(0)}_{obs} - g^{(0)}, ~~ \left.q_t\right|_{t=T} = \phi^{(1)}_{obs} - g^{(1)}.
\end{equation}

\section{Численная схема}
Рассмотрим приближенное решение задачи \ref{dir}--\ref{dirB}, аналогично строится схема решения задачи \ref{inv}--\ref{invB}.
Для дискретизации обобщенной формулировки по пространству используется метод конечных элементов с лагранжевыми элементами степени 2 на простейшей равномерной сетке из прямоугольных
треугольников. Для дискретизации по времени используется трехслойная схема, предложенная в статье \cite{scheme}, с параметром $\theta=\frac{1}{2}$. В итоге получаем
\begin{equation} \label{waveEqWeakDiscr}
\int_{\Omega} \partial^2_t \phi_n \nu \,d\Omega + \int_{\Omega} \left( \mu \nabla \phi_{n,\theta}
    \cdot \nabla \nu + b \phi_{n, \theta} \nu \right) \,d\Omega = \int_{\Omega}f_{n, \theta}\nu \,d\Omega ~~ \forall \nu \in X_h, ~~ \forall n \in \left[1, N\right],
\end{equation}
где $N$ -- число шагов по времени, $\tau$ -- шаг по времени, $r_{n,\theta} = \theta r_{n+1} + (1-2\theta)r_n + \theta
r_{n-1}$, $\partial^2_t r_n = (r_{n+1} - 2r_n + r_{n-1}) / \tau^2$.

Для расчетов использовался пакет FEniCS\cite{fenicsweb}.

\section{Результаты}
Тестирование проводилось при фиксированных параметрах $\mu = 1$, $b = 0$ и $b = 1$, $T = 1$ на единичном квадрате $(0,1) \times (0,1)$. Определим число шагов по времени $N = 50$, а число ячеек вдоль каждой из осей равным 25.

В случае, когда $b = 1$  и  решением задачи является функция
\begin{equation} \label{exact1}
\phi = \frac{1}{2}e^t sin(2\pi x) sin(\pi y),
\end{equation}
ошибка приближения $u_0$ в L2-норме равна 0.000231, ошибка приближения $u_1$ в L2-норме равна 0.00953.
Ошибка решения $\phi$ в норме $\max_{t \in [0,T]} \norm{\cdot}_{L^2}$ составила 0.000318.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{exact.png}
  \caption{Точное решение}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{nonexact.png}
  \caption{Приближенное решение}
  \label{fig:sub2}
\end{subfigure}
\caption{Графики $u_0$ для случая \ref{exact1}}
\label{fig:test}
\end{figure}

Также рассмотрим тестовую задачу при $b = 0$ и точным решением
\begin{equation} \label{exact2}
\phi = e^t(x - x^2) (y - y^2).
\end{equation}
Ошибка приближения $u_0$ в L2-норме равна 9.63e-06, ошибка приближения $u_1$ в L2-норме равна 0.000792.
Ошибка решения $\phi$ в норме $\max_{t \in [0,T]} \norm{\cdot}_{L^2}$ составила 9.63e-06 в момент времени $t=0$.

Следует отметить, что проверка корректности не ограничилась только этими двумя функциями. Также при сгущении сетки
и уменьшения шага по времени наблюдалось соответствующее падение ошибок решений.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{e2.png}
  \caption{Точное решение}
  \label{fig:sub11}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{ne2.png}
  \caption{Приближенное решение}
  \label{fig:sub21}
\end{subfigure}
\caption{Графики $u_0$ для случая \ref{exact2}}
\label{fig:test2}
\end{figure}

Попробуем решить более интересный пример восстановления начальных данных.
Фиксируем параметры $b = 1, N = 100$ и рассмотрим начальные условия
\begin{equation} \label{CC}
u_0 = \frac{1}{2} e^{-1000\left(\left(x- 0.3\right)^2 + \left(y-0.2\right)^2 \right)},
\end{equation}
\begin{equation}
u_1 = 0,
\end{equation}
которые описывают локализованное возмущение в точке $(0.3, 0.2)$ в начальный момент времени,
изображенное на \ref{exC}.
Также задается правая часть
\begin{equation}
f = 100 sin(4\pi t) sin(10\pi x) sin(10 \pi y).
\end{equation}
Нам неизвестно точное решение, поэтому для получения итогового наблюдения смоделируем распространение
волны, решив прямую задачу. На \ref{solutionT} приведены полученные графики решения в различные моменты времени.
Решение в момент времени $t=T\equiv 1$ принимается за финальное наблюдение $\Phi_{obs}$.
Восстановленное по  $\Phi_{obs}$ начальное условие $u_0$ изображено на \ref{C}.
Ошибка приближения $u_0$ в L2-норме оказалась равна 0.0285, ошибка приближения $u_1$ в L2-норме равна 0.51.
\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{exC.png}
  \caption{Исходная функция $u_0$}
  \label{exC}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{C.png}
  \caption{Восстановленная функция $u_0$}
  \label{C}
\end{subfigure}
\caption{Графики $u_0$ для случая \ref{CC}}
\label{figC}
\end{figure}

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{0p5.png}
  \caption{$t = 0.5$}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=1\linewidth]{0p6.png}
  \caption{$t = 0.6$}
\end{subfigure}
\caption{Приближенное решение $\phi$ в различные моменты времени для \ref{CC}}
\label{solutionT}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=0.8\linewidth]{final.png}
\caption{Финальное наблюдение $\phi_{obs}^{(0)}$ для \ref{CC}}
\label{final}
\end{figure}

\newpage
\begin{thebibliography}{30}
 \addcontentsline{toc}{section}{\bibname}

\bibitem{agoshkovMain}
Агошков В.И. Методы оптимального управления и сопряженных уравнений в задачах
математической физики. -- М.: ИВМ РАН, 2016. -- 244 с.

\bibitem{agoshkovPaper}
Agoshkov, V. I. (1995). Functional approaches to solving some inverse problems for second-order abstract equations. Journal of Inverse and Ill-Posed Problems, 3(5), 333-350.

\bibitem{lions1}
Лионс, Ж. Л. (1972). Оптимальное управление системами, описываемыми уравнениями с частными производными: Пер. с фр. Мир.

\bibitem{lions2}
Лионс, Ж. Л., \& Мадженес, Э. (1971). Неоднородные граничные задачи и их приложения: Пер. с франц. Мир.

\bibitem{scheme}
Dupont, T. (1973). L2-estimates for Galerkin methods for second order hyperbolic equations. SIAM journal on numerical analysis, 10(5), 880-889.

\bibitem{fenicsweb}
\url{https://fenicsproject.org}

\end{thebibliography}

\end{document}  
